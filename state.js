class Simulation {
    constructor(initialStates={}) {
        /* Rover state variables */
        this.battery = initialStates["initial-battery"];
        this.position = initialStates["deploy-point"];
        this.inventory = initialStates["inventory"];
        this.states = initialStates["states"];
        this.scenarios = initialStates["scenarios"];

        /* Environment state variables */
        this.temperature = initialStates["temperature"];
        this.humidity = initialStates["humidity"];
        this.solarFlare = initialStates["solar-flare"];
        this.storm = initialStates["storm"];
        this.areaMap = initialStates["area-map"];
        this.terrain = this.areaMap[this.position.row][this.position.column];

        this.isDead = false;
        this.messageFromRover = "";
        this.isStorm = false;
        this.stepsMovedAfterRecharge = 0;
        this.currentState = "normal";
    }

    getBatteryLevel() {
        return this.battery;
    }
    setBatteryLevel(newBatteryLevel) {
        this.battery = newBatteryLevel;
    }
    getPosition() {
        return this.position;
    }
    setPosition(newPosition) {
        this.position = newPosition;
    }
    getInventory() {
        return this.inventory;
    }
    setInventory(newInventory) {
        this.inventory = newInventory;
    }
    getTemperature() {
        return this.temperature;
    }
    setTemperature(newTemperature) {
        this.temperature = newTemperature;
    }
    getHumidity() {
        return this.humidity;
    }
    setHumidity(newHumidity) {
        this.humidity = newHumidity;
    }
    getSolarFlare() {
        return this.solarFlare;
    }
    setSolarFlare(newSolarFlare) {
        this.solarFlare = newSolarFlare;
    }
    getStorm() {
        return this.storm;
    }
    setStorm(newStorm) {
        this.storm = newStorm;
    }
    getTerrain() {
        return this.terrain;
    }
    setTerrain(newTerrain) {
        this.terrain = newTerrain;
    }
    setAreaMap(newAreaMap) {
        this.areaMap = newAreaMap;
    }
    setRover(newRoverStatus) {
        /* Rover state variables */
        this.battery = newRoverStatus["initial-battery"];
        this.position = newRoverStatus["deploy-point"];
        this.inventory = newRoverStatus["inventory"];
        this.states = newRoverStatus["states"];
        this.scenarios = newRoverStatus["scenarios"];
    }
    setEnvironment(newEnvironmentStatus) {
        this.temperature = newEnvironmentStatus["temperature"];
        this.humidity = newEnvironmentStatus["humidity"];
        this.solarFlare = newEnvironmentStatus["solar-flare"];
        this.storm = newEnvironmentStatus["storm"];
        this.areaMap = newEnvironmentStatus["area-map"];
        this.terrain = this.areaMap[this.position.row][this.position.column];
    }
    updateEnvironment(newValue) {
        let parameterMap = {
            "battery": "battery",
            "location": "position",
            "inventory": "inventory",
            "states": "states",
            "scenarios": "scenarios",
            "temperature": "temperature",
            "humidity": "humidity",
            "solar-flare": "solarFlare",
            "storm": "storm"
        }
        for(let parameter in newValue) {
            if(this.isDead == false) {
                this[parameterMap[parameter]] = newValue[parameter];
                if(parameter=="solar-flare" && newValue[parameter]) {
                    this.battery = 11;
                }
                if(parameter=="storm" && newValue[parameter]) {
                    if(this.inventory.length==0) {
                        this.isDead = true;
                        this.isStorm = true;
                    }
                    for (let itemIndex in this.inventory) {
                        let item = this.inventory[itemIndex];
                        if(item.type == "storm-shield" && item.quantity>=1) {
                            item.quantity = item.quantity-1;
                            this.isStorm = true;
                            if(item.quantity==0) {
                                this.inventory.splice(Number(itemIndex), 1);
                            }
                        }
                        else {
                            this.isDead = true;
                            this.isStorm = true;
                        }
                    }
                }
                if(parameter=="storm" && newValue[parameter] == false) {
                    this.isStorm = false;
                    this.messageFromRover = "";
                }
            }
        }
    }
    moveRover(direction={}) {
        let newDirection = direction.direction;
        this.messageFromRover = "";
        let directionMap = {
            "up": "up",
            "down": "down",
            "left": "left",
            "right": "right"
        }
        if(directionMap[newDirection] && this.isDead == false && this.isStorm==false &&this.currentState=="normal") {
            let newXAxis = this.position.column;
            let newYAxis = this.position.row;
            if(newDirection=="up") {
                newYAxis = this.position.row - 1;  
            }
            if(newDirection=="down") {
                newYAxis = this.position.row + 1;  
            }
            if(newDirection=="left") {
                newXAxis = this.position.column - 1;  
            }
            if(newDirection=="right") {
                newXAxis = this.position.column + 1;  
            }
            if(this.areaMap[newXAxis] && this.areaMap[newXAxis][newYAxis]) {
                this.battery = this.battery-1;
                ++this.stepsMovedAfterRecharge;
                if(this.stepsMovedAfterRecharge==10) {
                    this.battery = 10;
                    this.stepsMovedAfterRecharge = 0;
                }
                if(this.battery<1) {
                    this.isDead = true;
                }
                this.position.row = newYAxis;
                this.position.column = newXAxis;
                this.terrain = this.areaMap[this.position.row][this.position.column];
                for(let scenario of this.scenarios) {
                    for(let actionIndex in scenario["conditions"]) {
                        let action = scenario["conditions"][actionIndex];
                        if(action.type=="environment") {
                            let lhs = this[action.property];
                            let rhs = action.value;
                            let operators = {
                                "eq": lhs==rhs,
                                "ne": lhs!=rhs,
                                "lte": lhs<=rhs,
                                "gte": lhs>=rhs,
                                "lt": lhs<rhs,
                                "gt": lhs>rhs
                            };
                            let priority = {
                                "water-sample": 2,
                                "rock-sample": 3
                            }
                            console.log(action.property, lhs, action.value, rhs, operators[action.operator])
                            if(operators[action.operator]) {
                             let toDo = scenario["rover"][actionIndex]["performs"]["collect-sample"];
                             for(let state of this.states) {
                                if(state["name"]==this.currentState && toDo) {
                                    if(state["allowedActions"].indexOf("collect-sample")>-1) {
                                        if(toDo.type=="water-sample" || toDo.type=="rock-sample") {
                                            this.inventory.push({
                                                "type": toDo.type,
                                                "qty": toDo.qty,
                                                "priority": priority[toDo.type]
                                            })
                                        }
                                    }
                                }
                             }
                            }
                        }
                    }
                }
            }
            else {
                console.log(newXAxis, newYAxis, "in", 0>newYAxis || newYAxis > this.areaMap[this.areaMap.length-1].length, 0>newXAxis || newXAxis > this.areaMap.length-1)
                if(0>newYAxis || newYAxis > this.areaMap[this.areaMap.length-1].length-1) {
                    this.messageFromRover = "Can move only within mapped area";
                }
                if(0>newXAxis || newXAxis > this.areaMap.length-1) {
                    this.messageFromRover = "Can move only within mapped area";
                }
            }
        }
        else if(this.isStorm) {
            this.messageFromRover = "Cannot move during a storm";
        }
    }
    getTotalStatus() {
        let rover = {};
        let environment = {}; 
        rover["battery"] = this.battery;
        rover["location"] = this.position;
        rover["inventory"] = this.inventory;
        // rover["states"] = this.states;
        // rover["scenarios"] = this.scenarios;


        environment["temperature"] = this.temperature;
        environment["humidity"] = this.humidity;
        environment["solar-flare"] = this.solarFlare;
        environment["storm"] = this.storm;
        environment["terrain"] = this.areaMap[this.position.row][this.position.column];

        return {rover, environment, "dead": this.isDead, "message": this.messageFromRover};
    }
}

module.exports = {
    Simulation
}