const express = require('express');
const simulation = require('./state.js').Simulation;
const initialStates = require('./initialStateValues');
const bodyParser = require('body-parser')
const checkStatusAndReturn = require('./helpers').checkStatusAndReturn;

const app = express();
app.use(bodyParser.json()) // for parsing application/json

let rover = new simulation({...initialStates.environment, ...initialStates.rover});

app.post('/api/environment/configure', (req, res) => {
    rover.setEnvironment(req.body);
    if(checkStatusAndReturn(rover)) {
        res.sendStatus(200);
    }
})

app.post('/api/rover/configure', (req, res) => {
    rover.setRover(req.body);
    if(checkStatusAndReturn(rover)) {
        res.sendStatus(200);
    }
})

app.get('/api/rover/status', (req, res) => {
    let response = checkStatusAndReturn(rover, true);
    if(typeof response =="object") {
        res.status(200).send(response.partialObject);
    }
})

app.patch('/api/environment', (req, res) => {
    rover.updateEnvironment(req.body);
    if(checkStatusAndReturn(rover)) {
        res.sendStatus(200);
    }
})

app.post('/api/rover/move', (req, res) => {
    rover.moveRover(req.body);
    let response = checkStatusAndReturn(rover);
    if(typeof response =="object" && response.message) {
        res.status(428).send(response);
    }
    else if(typeof response =="object") {
        res.sendStatus(200);
    }
})

app.listen(3000, () => {
    console.log('Server is up on port 3000.')
})