* This is a Node.js based application. So please install node and npm (usually bundled with node) before running this project.
* All the dependencies/plugins used are open source (from npm repo)
* `express` web server and other express compatible middlewares are used
* The zipped version has all the dependencies (node_modules). If git repo is being used, run `npm install` first to resolve all the dependencies after clone/download
* Simply run `npm start` to start the application
* Since the input and output is completely JSON prefer using an API tester like Postman to test/use this application
* The default port that it runs on is "3000". This can be changed by changing the value in `app.js` file present in root folder of project (search for the line "app.listen")
* Hit "localhost:3000" followed by the valid routes as below and supply necessary data
    * post - /api/environment/configure
    * post - /api/rover/configure
    * get - /api/rover/status
    * patch - /api/environment
    * post - /api/rover/move

