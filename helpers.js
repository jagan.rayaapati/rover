function checkStatusAndReturn(rover, mandatoryStatus = false) {
    let simulationStatus = rover.getTotalStatus();
    if(simulationStatus.dead) {
        return false;
    }
    else {
        if(simulationStatus.message) {
            const {message, ...partialObject} = simulationStatus;
            const subset = {message};
            if(mandatoryStatus) {
                return {subset, partialObject};
            }
            else {
                return subset
            }
            // res.status(200).send(subset);
        }
        else {
            const {message, ...partialObject} = simulationStatus;
            return {partialObject};
            // res.status(200).send(partialObject);
        }
    }
}

module.exports= {
    checkStatusAndReturn
}